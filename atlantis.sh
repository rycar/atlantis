#!/bin/bash

STARTDIR=`pwd`

USER=`whoami`
if [ $USER != "root" ];
then
    sudo bash -c "$0"
    exit 0;
fi

if [ ! -f "/usr/bin/pv" ];
then
    apt-get update
else
    bash -c "apt-get update >&1 | pv -l > /dev/null"
fi

if [ ! -f "/usr/bin/puppet" ];
then
    echo ""
    echo "Installing puppet..."
    apt-get -y install puppet
fi

if [ ! -f "/usr/bin/git" ];
then
    echo ""
    echo "Installing git..."
    apt-get -y install git
fi

if [ -d "/etc/puppet" ];
then
    if [ ! -d "/etc/puppet/.git" ];
    then
        echo ""
        echo "Installing Atlantis..."
        rm -Rf /etc/puppet
        git clone https://bitbucket.org/rycar/atlantis.git /etc/puppet
    fi
fi

cd /etc/puppet
git pull

cd /etc/puppet/manifests
puppet apply /etc/puppet/manifests/atlantis.pp

cd "$STARTDIR"