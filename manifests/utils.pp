class utils {
    $packages = [ "pv", "screen", "mc", "curl", "psmisc", "p7zip" ]
    package { $packages: ensure => "latest" }
}
